﻿using DependencyInjectionDemo.Library;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace DependencyInjectionDemo.LibraryTests
{
  public class ParserSpecs
  {
    [Fact]
    public void ParserHandlesExpectedInput()
    {
      var parser = new DefaultInputParser();

      var success = parser.TryParse("1 book at 12.49", out var count, out var name, out var price);

      Assert.True(success);
      Assert.Equal(1, count);
      Assert.Equal("book", name);
      Assert.Equal(12.49M, price);
    }

    [Fact]
    public void ParserHandlesMultipleWords()
    {
      var parser = new DefaultInputParser();

      var success = parser.TryParse("1 imported box of chocolates at 10.00", out var count, out var name, out var price);

      Assert.True(success);
      Assert.Equal(1, count);
      Assert.Equal("imported box of chocolates", name);
      Assert.Equal(10.00M, price);
    }
  }
}
