﻿using DependencyInjectionDemo.Library;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace DependencyInjectionDemo.LibraryTests
{
  public class ProductCatalogSpecs
  {
    [Fact]
    public void ProductCatalogReturnsProducts()
    {
      var productCatalog = new InMemoryProductCatalog();

      productCatalog.Add(new Product { Category = TaxCategory.Other, Imported = false, Name = "Sample", Price = 12.34M });

      var found = productCatalog.TryFindProductId("Sample", 12.34M, out var productId);

      var returnedProduct = productCatalog.GetProductById(productId);

      Assert.True(found);
      Assert.Equal("Sample", returnedProduct.Name);
    }

    [Fact]
    public void ProductCatalogIsCaseInsensitive()
    {
      var productCatalog = new InMemoryProductCatalog();

      productCatalog.Add(new Product { Category = TaxCategory.Other, Imported = false, Name = "Sample", Price = 12.34M });

      var found = productCatalog.TryFindProductId("sAmPle", 12.34M, out var productId);

      var returnedProduct = productCatalog.GetProductById(productId);

      Assert.True(found);
      Assert.Equal("Sample", returnedProduct.Name);
    }

    [Fact]
    public void ProductMustMatchPrice()
    {
      var productCatalog = new InMemoryProductCatalog();

      productCatalog.Add(new Product { Category = TaxCategory.Other, Imported = false, Name = "Sample", Price = 12.34M });

      var found = productCatalog.TryFindProductId("Sample", 0.00M, out var productId);

      var returnedProduct = productCatalog.GetProductById(productId);

      Assert.False(found);
    }
  }
}
