using DependencyInjectionDemo.Library;
using System;
using Xunit;

namespace DependencyInjectionDemo.LibraryTests
{
  public class TaxRateSpecs
  {
    [Fact]
    public void TaxPolicyRoundsUpToNearest5Cents()
    {
      var taxPolicy = new DefaultTaxPolicy();

      var tax = taxPolicy.GetTax(new Product { Category = TaxCategory.Other, Imported = false, Price = 14.99M });

      Assert.Equal(1.50M, tax);
    }

    [Fact]
    public void BooksAreExempt()
    {
      var taxPolicy = new DefaultTaxPolicy();

      var tax = taxPolicy.GetTax(new Product { Category = TaxCategory.Books, Imported = false, Price = 12.49M });

      Assert.Equal(0.00M, tax);
    }

    [Fact]
    public void FoodIsExempt()
    {
      var taxPolicy = new DefaultTaxPolicy();

      var tax = taxPolicy.GetTax(new Product { Category = TaxCategory.Food, Imported = false, Price = 0.85M });

      Assert.Equal(0.00M, tax);
    }

    [Fact]
    public void ImportsHave10PercentTax()
    {
      var taxPolicy = new DefaultTaxPolicy();

      var tax = taxPolicy.GetTax(new Product { Category = TaxCategory.Food, Imported = true, Price = 10.00M });

      Assert.Equal(0.50M, tax);
    }
  }
}
