﻿using DependencyInjectionDemo.Library;
using FakeItEasy;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xunit;

namespace DependencyInjectionDemo.LibraryTests
{
  public class PrinterSpecs
  {
    [Fact]
    public void OutputIsInExpectedFormat()
    {
      using (var output = new StringWriter())
      {
        var productId = Guid.NewGuid();
        var printer = new DefaultRecieptPrinter(output, CreateCatalog(productId), CreateTaxPolicy());
        printer.Print(new Purchase { PurchasedProducts = { productId } });

        var expected = @"Product: $12.34
Sales Tax: $0.00
Total: $12.34";

        Assert.Equal(expected, output.ToString());
      }
    }

    [Fact]
    public void OutputCombinesMultipleProducts()
    {
      using (var output = new StringWriter())
      {
        var productId = Guid.NewGuid();
        var printer = new DefaultRecieptPrinter(output, CreateCatalog(productId), CreateTaxPolicy());
        printer.Print(new Purchase { PurchasedProducts = { productId, productId } });

        var expected = @"Product: $24.68 (2 @ $12.34)
Sales Tax: $0.00
Total: $24.68";

        Assert.Equal(expected, output.ToString());
      }
    }


    IProductCatalog CreateCatalog(Guid productId)
    {
      var catalog = A.Fake<IProductCatalog>();

      Guid _;

      A.CallTo(() => catalog.TryFindProductId("Product", 12.34M, out _)).Returns(true).AssignsOutAndRefParameters(productId);
      A.CallTo(() => catalog.GetProductById(productId)).Returns(new Product { Category = TaxCategory.Other, Imported = false, Price = 12.34M, Name = "Product" });

      return catalog;
    }

    ITaxPolicy CreateTaxPolicy()
    {
      var taxPolicy = A.Fake<ITaxPolicy>();
      A.CallTo(() => taxPolicy.GetTax(A<Product>._)).Returns(0M);
      return taxPolicy;
    }
  }
}
