﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DependencyInjectionDemo.Library
{
  /// <summary>
  /// Defines a type of product which can be purchased
  /// </summary>
  public sealed class Product : IProduct
  {
    public string Name { get; set; }
    public decimal Price { get; set; }
    public bool Imported { get; set; }
    public TaxCategory Category { get; set; }
  }
}
