﻿namespace DependencyInjectionDemo.Library
{
  public interface ITaxPolicy
  {
    decimal GetTax(IProduct product);
  }
}