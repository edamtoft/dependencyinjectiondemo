﻿namespace DependencyInjectionDemo.Library
{
  public enum TaxCategory
  {
    Other = 0,
    Books,
    Food,
    Medical,
  }
}