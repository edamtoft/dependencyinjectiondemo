﻿namespace DependencyInjectionDemo.Library
{
  public interface IRecieptPrinter
  {
    void Print(Purchase purchase);
  }
}