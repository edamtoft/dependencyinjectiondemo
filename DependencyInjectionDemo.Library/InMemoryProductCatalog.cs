﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DependencyInjectionDemo.Library
{
  /// <summary>
  /// Stores a set of products in an in-memory backing dictionary
  /// </summary>
  public sealed class InMemoryProductCatalog : IProductCatalog, IEnumerable<IProduct>
  {
    private readonly Dictionary<Guid, IProduct> _backingDictionary = new Dictionary<Guid, IProduct>();

    public IProduct GetProductById(Guid productId)
    {
      return _backingDictionary.TryGetValue(productId, out var product) ? product : null;
    }

    public void Add(IProduct product)
    {
      _backingDictionary.Add(Guid.NewGuid(), product);
    }

    public bool TryFindProductId(string name, decimal price, out Guid productId)
    {
      foreach (var entry in _backingDictionary)
      {
        if (entry.Value.Name.Equals(name, StringComparison.OrdinalIgnoreCase) && entry.Value.Price == price)
        {
          productId = entry.Key;
          return true;
        }
      }
      productId = default(Guid);
      return false;
    }


    IEnumerator IEnumerable.GetEnumerator() => ((IEnumerable)_backingDictionary.Values).GetEnumerator();
    IEnumerator<IProduct> IEnumerable<IProduct>.GetEnumerator() => _backingDictionary.Values.GetEnumerator();


  }
}
