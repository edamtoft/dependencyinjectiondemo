﻿using System;

namespace DependencyInjectionDemo.Library
{
  public interface IProductCatalog
  {
    IProduct GetProductById(Guid productId);

    bool TryFindProductId(string name, decimal price, out Guid productId);
  }
}