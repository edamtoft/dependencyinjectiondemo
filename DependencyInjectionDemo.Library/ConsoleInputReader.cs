﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace DependencyInjectionDemo.Library
{
  /// <summary>
  /// Reads a purchase from the console input and prompts the user to retry if the format is invalid
  /// </summary>
  public sealed class ConsoleInputReader : IInputReader
  {
    private readonly IProductCatalog _catalog;
    private readonly IInputParser _parser;

    public ConsoleInputReader(IProductCatalog catalog, IInputParser parser)
    {
      _catalog = catalog;
      _parser = parser;
    }

    public Purchase ReadInput()
    {
      Console.WriteLine("Enter purchase below. Enter an empty line to finish.");
      var purchase = new Purchase();
      string line;
      while (!string.IsNullOrWhiteSpace(line = Console.ReadLine()))
      {
        if (!_parser.TryParse(line, out var count, out var name, out var price))
        {
          Console.WriteLine("Invalid Format. Please try again.");
          continue;
        }

        if (!_catalog.TryFindProductId(name, price, out var productId))
        {
          Console.WriteLine("No matching product not found in the catalog. Please Try Again.");
          continue;
        }

        purchase.PurchasedProducts.AddRange(Enumerable.Repeat(productId, count));
      }
      return purchase;
    }
  }
}
