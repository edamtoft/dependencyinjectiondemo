﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DependencyInjectionDemo.Library
{
  /// <summary>
  /// Writes a formatted reciept to an output stream
  /// </summary>
  public sealed class DefaultRecieptPrinter : IRecieptPrinter
  {
    private readonly TextWriter _out;
    private readonly IProductCatalog _catalog;
    private readonly ITaxPolicy _taxPolicy;

    public DefaultRecieptPrinter(TextWriter output, IProductCatalog catalog, ITaxPolicy taxPolicy)
    {
      _out = output;
      _catalog = catalog;
      _taxPolicy = taxPolicy;
    }

    public void Print(Purchase purchase)
    {
      var items = (from productId in purchase.PurchasedProducts
                   group productId by productId into productsById
                   let product = _catalog.GetProductById(productsById.Key)
                   let tax = _taxPolicy.GetTax(product)
                   let count = productsById.Count()
                   let unitTaxTotal = tax * count
                   let unitPriceWithTax = product.Price + tax
                   let unitTotalWithTax = unitPriceWithTax * count
                   select new { product, tax, unitPriceWithTax, unitTaxTotal, unitTotalWithTax, count }).ToList();

      foreach (var item in items)
      {
        var itemTotalCost = item.product.Price * item.count;
        var lineItem = item.count == 1
          ? $"{item.product.Name}: {item.unitTotalWithTax:C}"
          : $"{item.product.Name}: {item.unitTotalWithTax:C} ({item.count} @ {item.unitPriceWithTax:C})";

        _out.WriteLine(lineItem);
      }

      var totalTax = items.Sum(item => item.unitTaxTotal);
      var totalTaxedPrice = items.Sum(item => item.unitTotalWithTax);

      _out.WriteLine($"Sales Tax: {totalTax:C}");
      _out.Write($"Total: {totalTaxedPrice:C}");
    }
  }
}
