﻿namespace DependencyInjectionDemo.Library
{
  public interface IInputReader
  {
    Purchase ReadInput();
  }
}