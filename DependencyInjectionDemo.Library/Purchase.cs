﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DependencyInjectionDemo.Library
{
  /// <summary>
  /// A set of individual items being purchased using a Guid which can be looked up in the product catalog
  /// </summary>
  public sealed class Purchase
  {
    public List<Guid> PurchasedProducts { get; } = new List<Guid>();
  }
}
