﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DependencyInjectionDemo.Library
{
  /// <summary>
  /// Applies tax based on code test rules:
  /// Basic sales tax is applicable at a rate of 10% on all goods, 
  /// except books, food, and medical products that are exempt. 
  /// Import duty is an additional sales tax applicable on all 
  /// imported goods at a rate of 5%, with no exemptions.
  /// </summary>
  public sealed class DefaultTaxPolicy : ITaxPolicy
  {
    public decimal GetTax(IProduct product)
    {
      var baseRate = GetRate(product);
      var baseRateWithImportDuty = AddImportDuty(product, baseRate);
      var rawTax = baseRateWithImportDuty * product.Price;
      return RoundUpToNearest5(rawTax);
    }

    private decimal GetRate(IProduct product)
    {
      switch (product.Category)
      {
        case TaxCategory.Books:
        case TaxCategory.Food:
        case TaxCategory.Medical:
          return 0.00M;
        default:
          return 0.10M;
      }
    }

    private decimal AddImportDuty(IProduct product, decimal baseRate) => product.Imported ? baseRate + 0.05M : baseRate;

    private decimal RoundUpToNearest5(decimal price) => Math.Ceiling(price / 0.05M) * 0.05M;
  }
}
