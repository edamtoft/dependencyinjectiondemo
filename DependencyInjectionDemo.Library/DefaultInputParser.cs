﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace DependencyInjectionDemo.Library
{
  /// <summary>
  /// Parses a string in the format described by the code test documentation
  /// </summary>
  public sealed class DefaultInputParser : IInputParser
  {
    private static readonly Regex InputPattern = new Regex(@"^(\d+) (.*) at (\d+\.\d{2})\s*$", RegexOptions.IgnoreCase | RegexOptions.Compiled);

    public bool TryParse(string input, out int count, out string name, out decimal price)
    {
      var match = InputPattern.Match(input);

      if (match.Success && int.TryParse(match.Groups[1].Value, out count) && decimal.TryParse(match.Groups[3].Value, out price))
      {
        name = match.Groups[2].Value;
        return true;
      }

      count = default(int);
      name = default(string);
      price = default(decimal);
      return false;
    }

  }
}
