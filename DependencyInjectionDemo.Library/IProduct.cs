﻿namespace DependencyInjectionDemo.Library
{
  public interface IProduct
  {
    TaxCategory Category { get; }
    bool Imported { get; }
    string Name { get; }
    decimal Price { get; }
  }
}