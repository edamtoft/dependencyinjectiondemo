﻿namespace DependencyInjectionDemo.Library
{
  public interface IInputParser
  {
    bool TryParse(string input, out int count, out string name, out decimal price);
  }
}