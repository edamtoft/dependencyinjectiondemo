﻿using DependencyInjectionDemo.Library;
using System;
using System.Collections.Generic;
using System.Text;

namespace DependencyInjectionDemo
{
  /// <summary>
  /// This is the place where we piece together all of the dependencies. Ideally, there is only one
  /// place in the entire application where this happens. In a more complex example, this would usually take some sort of configuration
  /// which would determine how the program is composed (I.E. use an in-memory cache when in a development environemnt or whatever)
  /// </summary>
  internal static class CompositionRoot
  {
    public static void Compose(out IRecieptPrinter printer, out IInputReader reader)
    {
      var taxPolicy = new DefaultTaxPolicy();

      var productCatalog = new InMemoryProductCatalog
      {
        new Product { Category = TaxCategory.Books, Price = 12.49M, Imported = false, Name = "Book" },
        new Product { Category = TaxCategory.Other, Price = 14.99M, Imported = false, Name = "Music CD" },
        new Product { Category = TaxCategory.Food, Price = 0.85M, Imported = false, Name = "Chocolate Bar" },
        new Product { Category = TaxCategory.Food, Price = 10.00M, Imported = true, Name = "Imported Box of Chocolates" },
        new Product { Category = TaxCategory.Other, Price = 47.50M, Imported = true, Name = "Imported Bottle of Perfume" },
        new Product { Category = TaxCategory.Other, Price = 27.99M, Imported = true, Name = "Imported Bottle of Perfume" },
        new Product { Category = TaxCategory.Other, Price = 18.99M, Imported = false, Name = "Bottle of Perfume" },
        new Product { Category = TaxCategory.Medical, Price =9.75M, Imported = false, Name = "Packet of Headache Pills" },
        new Product { Category = TaxCategory.Food, Price = 11.25M, Imported = true, Name = "Box of Imported Chocolates" },
      };

      printer = new DefaultRecieptPrinter(Console.Out, productCatalog, taxPolicy);

      var parser = new DefaultInputParser();
      reader = new ConsoleInputReader(productCatalog, parser);

    }
  }
}
