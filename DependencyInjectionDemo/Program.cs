﻿using DependencyInjectionDemo.Library;
using System;

namespace DependencyInjectionDemo
{
  static class Program
  {
    static void Main(string[] args)
    {
      CompositionRoot.Compose(out var recieptPrinter, out var reader);

      var purchase = reader.ReadInput();
      recieptPrinter.Print(purchase);

      Console.Read();
    }
  }
}
